package view;

import controller.AlienDefenceController;
import model.persistence.IPersistence;
import model.persistenceDummy.PersistenceDummy;
import view.menue.MainMenue;

public class StartAlienDefence {

	public static void main(String[] args) {
		
		IPersistence 		   alienDefenceModel      = new PersistenceDummy();
		AlienDefenceController alienDefenceController = new AlienDefenceController(alienDefenceModel);
		MainMenue              mainMenue              = new MainMenue(alienDefenceController);
		
		mainMenue.setVisible(true);
	}
	
	/*
	 * Erkl�rung zu Schichtenmodell:
	 * 
	 * Das ist vermutlich die 3-Schicht-Architektur (und als Spezialfall die MVC-Architektur).
	 * 
	 * 				View
	 * Controller
	 * 				Model
	 * 
	 *View (Pr�sentationsschicht)
	 * Zust�ndig f�r die ben�tigten Daten aus dem Modell und die Entgegennahme von Benutzerinteraktionen.
	 * Wird �ber �nderungen der Daten im Modell unterrichtet und kann dann die aktualisierten Daten abrufen.
	 * 
	 *Model (Datenhaltung)
	 * Enth�lt die Daten und verwaltet diese
	 *
	 *Controller (Fachkonzeptschicht)
	 * Stellt die Steuerung f�r die View dar. Entweder veranlasst er �nderungen in der Pr�sentation (z.B. Fenster verschiebt sich) oder er bereitet
	 * Daten zwischen View und Model auf und �bertr�gt sie (z.B. Berechnung von Daten und EIntragung des Ergebnisses in die Datenbank).
	 * 
	 */
	
}


